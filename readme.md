# Node Server
## Before You Start
Before you start learning about the *Node* server you should have some basic knowledge about the following topics

- [JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript)

## What You Need
If you want to develop applications on *Node*, then you need the following programs and tools

- [Node.js](https://nodejs.org/en)
- [npm](https://www.npmjs.com/)

and some IDE, such as [Visual Sudio Code](https://code.visualstudio.com/) or some other tool or editor.

- [GIT](https://git-scm.com/) is used for version control.

# Demos and Exercises
The demos and exercises are organized in *GIT* branches. The demo branches start with *demo/...* and the exercise branches start with *exercise/...*

## The Trail
The trail is a good way to learn things step by step. The demos of the trail can be found under *demo/trail/...* and the exercises under *exercise/trail/...*

This is the sequence, in which the trail is organized

1. *demo/trail/rest*
1. *demo/trail/sse*
